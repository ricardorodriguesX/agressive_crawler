pub struct Page {
    title: String,
    url: String,
    image: String,
    links: Array
}
