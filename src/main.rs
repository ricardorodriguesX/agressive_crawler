use scraper::{Html, Selector};

static mut VERBOSE: bool = false;

fn main() 
{
    let args: Vec<String> = std::env::args().collect();

    let mut i = 1;
    
    // let mut j = 0;

    let mut should_parse: bool = true;

    let mut arg: &str;

    while i < args.len() && (args[i].starts_with("-") || args[i].starts_with("--")){
        
        arg = &args[i];

        match arg {
            "-h" | "--help" =>{
                println!("Usage: aggressive_crawler [OPTIONS] [command] url");
                
                should_parse = false;
            },
            "-v" | "--verbose" =>{
                println!("Verbose enabled");
                
                unsafe {VERBOSE = true;}
            },
            _ => panic!("Malformed command, use --help to see usage")
        }

        if !should_parse {
            return;
        }

        i += 1;
    }

    if i == args.len(){
        panic!("Malformed command, use --help to see usage")
    }

    crawl(&args[i]);
}

fn request_maker(url: &str) -> Result<String, ureq::Error>
{
    let body: String = ureq::get(url)
        .set("Example-Header", "header value")
        .call()?
        .into_string()?;

    Ok(body)
}

fn crawl(url: &str) -> String 
{
    let html = match request_maker(url) { 
        Ok(body) => body, 
        Err(e) => {
            if unsafe {VERBOSE} {
                eprintln!("Error from URL {}: {}", e, url); 
            }
            
            return String::from("");}
    };

    if unsafe {VERBOSE} {
        println!("{}", html);
    }

    let document = Html::parse_document(&html);

    let selector = Selector::parse("a").unwrap();

    for element in document.select(&selector) {

        let mut link = String::from(element.value().attr("href").unwrap());

        if link.starts_with("//") {
            link = build_link(link);
        }

        return crawl(&link);
    }

    return String::from(url);
}

fn build_link(link: String) -> String
{
    let mut data = String::from("http:");

    if link.starts_with("//"){
        return data + &link;
    }

    if link.starts_with("/") {

        data = String::from("http:/");

        return data + &link;
    }

    return String::from(link);
}